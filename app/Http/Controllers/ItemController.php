<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;

class ItemController extends Controller
{
    //
    public function index(){
        $client = new Client();
        $booksResponse = $client->request('GET', 'http://localhost/provider1/books');
        $magazinesResponse = $client->request('GET', 'http://localhost/provider2/books');

        $books = json_decode($booksResponse->getBody(), true);
        $magazines = json_decode($magazinesResponse->getBody(), true);
        $items = array_merge($this->convertBooks($books), $this->convertMagazines($magazines));
        shuffle($items);
        return response()->json($items);
        return $items;

    }
    public function show($id){
        return response()->json([]);
    }

    private function convertBooks($books){
        $result = [];
        foreach ($books as $book){
            $item = [
                'Numer ksiazki' => $book['ID'],
                'Tytul ksiazki' => $book['Tytul'],
                'Autor ksiazki' => $book['Autor'],
				'Rodzaj okladki' => $book['Okladka'],
            ];
            array_push($result, $item);
        }
        return $result;
    }
    private function convertMagazines($magazines){
        $result = [];
        foreach ($magazines as $magazine){
            $item = [
                'Numer ksiazki' => $magazine['ID'],
                'Tytul ksiazki' => $magazine['Tytul'],
                'Autor ksiazki' => $magazine['Autor'],
				'Liczba stron' => $magazine['Strony'],
                'Wydawnictwo' => $magazine['Wydawnictwo'],
            ];
            array_push($result, $item);
        }
        return $result;
    }
}
